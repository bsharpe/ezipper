defmodule MiniZipper.EndpointTest do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts MiniZipper.Endpoint.init([])

  test "it returns pong" do
    conn = conn(:get, "/ping")

    conn = MiniZipper.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "pong!"
  end

  test "it returns 200 with a valid payload" do
    conn = conn(:post, "/process", %{urls: [
      %{ url: "https://media.giphy.com/media/3oz8xD0xvAJ5FCk7Di/giphy.gif",
         filename: "pic001.gif"
      }
    ]} )

    conn = MiniZipper.Endpoint.call(conn, @opts)

    assert conn.status == 200
  end

  test "it returns 422 with an invalid payload" do
    conn = conn(:post, "/process", %{} )

    conn = MiniZipper.Endpoint.call(conn, @opts)

    assert conn.status == 422
  end

  test "it returns 404 when no route matches" do
    conn = conn(:get, "/fail")

    conn = MiniZipper.Endpoint.call(conn, @opts)

    assert conn.status == 404
  end
end
