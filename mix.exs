defmodule MiniZipper.MixProject do
  use Mix.Project

  def project do
    [
      app: :mini_zipper,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      build_embedded: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison],
      mod: {MiniZipper.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug_cowboy, "~> 2.0"}, # This will pull in Plug AND Cowboy
      {:jason, "~> 1.0"},       # JSON lib
      {:httpoison, "~> 1.6"},   # HTML lib
      {:zstream, "~> 0.2"}
    ]
  end
end
