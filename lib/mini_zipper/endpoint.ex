defmodule MiniZipper.Endpoint do
  @moduledoc """
  A Plug responsible for
   - logging request info,
   - parsing request body's as JSON,
   - matching routes, and
   - dispatching responses.
  """

  use Plug.Router

  # This module is a Plug, that also implements it's own plug pipeline, below:

  # Using Plug.Logger for logging request information
  plug(Plug.Logger)
  # responsible for matching routes
  plug(:match)
  # Using Jason for JSON decoding
  # Note, order of plugs is important, by placing this _after_ the 'match' plug,
  # we will only parse the request AFTER there is a route match.
  plug(Plug.Parsers, parsers: [:json], json_decoder: Jason)
  # responsible for dispatching responses
  plug(:dispatch)

  # A simple route to test that the server is up
  # Note, all routes must return a connection as per the Plug spec.
  get "/ping" do
    send_resp(conn, 200, "pong!")
  end

  # Handle incoming urls, if the payload is the right shape, process the
  # urls, otherwise return an error.
  post "/process" do
    {status, body} =
      case conn.body_params do
        %{"urls" => urls} -> process_urls(urls)
        _ -> {422, missing_urls()}
      end

    if status == 200 do
      # Setup the headers
      chunked_conn =
        conn
        |> put_resp_content_type("application/zip")
        |> put_resp_header("content-disposition","attachment; filename=\"archive.zip\"")
        |> put_resp_header("content-type", "application/octet-stream")
        |> send_chunked(status)

      # Stream the zip archive as it's being processed by Zstream()
      # Halt if the client closes the connection.
      Enum.reduce_while(Zstream.zip(body), chunked_conn, fn (chunk, conn) ->
        case Plug.Conn.chunk(chunked_conn, chunk) do
          {:ok, conn} ->
            {:cont, conn}
          {:error, :closed} ->
            {:halt, conn}
        end
      end)

      chunked_conn
    else
      send_resp(conn, status, body)
    end

  end

  defp process_urls(urls) when is_list(urls) do
    urls = for e <- urls do
      Zstream.entry(e["filename"], HTTPStream.get(e["url"]))
    end
    {200, urls}
  end

  defp process_urls(_) do
    {422, Jason.encode!(%{response: "Please Send Some URLs!"})}
  end

  defp missing_urls do
    Jason.encode!(%{error: "Expected Payload: { 'urls': [{ 'url': '', 'filename': ''},...] }"})
  end

  # A catchall route, 'match' will match no matter the request method,
  # so a response is always returned, even if there is no route to match.
  match _ do
    send_resp(conn, 404, "End of Line")
  end
end
