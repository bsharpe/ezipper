# Elixir URL-2-ZIP Service

## Setup
- install Erlang v22.1.4  `brew install erlang`
- install Elixir v1.9.1   `brew install elixir`

Navigate to the project folder:

- install dependencies    `mix deps.get`
- run the server locally  `mix run --no-halt`

## Usage

POST an array of URL/Filename maps to `/process` and you'll receive a zip archive of those urls in a file called `archive.zip`

## Testing
`> mix test`

## Example
NOTE: This calls the app, already running on Heroku

```
curl -X POST \
  http://whispering-cove-75699.herokuapp.com/process \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "urls": [
  {
    "url": "https://media.giphy.com/media/3oz8xD0xvAJ5FCk7Di/giphy.gif",
    "filename": "pic001.gif"
  },
  {
    "url": "https://media.giphy.com/media/l3vRfhFD8hJCiP0uQ/giphy.gif",
    "filename": "pic002.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xG0CiDpXqYXCz6/giphy.gif",
    "filename": "pic003.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xG0aignBvOhIMU/giphy.gif",
    "filename": "pic004.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xwooUvMqNB1zEs/giphy.gif",
    "filename": "pic005.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xyB3C126ZDDAuk/giphy.gif",
    "filename": "pic006.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xSwPT41eZOvS2A/giphy.gif",
    "filename": "pic007.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xAsuv5apu2cVws/giphy.gif",
    "filename": "pic008.gif"
  },
  {
    "url": "https://media.giphy.com/media/l3vR7ACppQS71ngUU/giphy.gif",
    "filename": "pic009.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xSD5WkRNG1R6x2/giphy.gif",
    "filename": "pic010.gif"
  },
  {
    "url": "https://media.giphy.com/media/3oz8xzYXuCWF1IXv68/giphy.gif",
    "filename": "pic011.gif"
  },
  {
    "url": "https://media.giphy.com/media/l3vRfjcp7VMSZwbGo/giphy.gif",
    "filename": "pic012.gif"
  }
]

}' > archive.zip
```
